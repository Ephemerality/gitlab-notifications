version = $(shell cat src/manifest.json | grep version | awk -F'"' '{print $$4}')

build:
	@npm run build
	@make zip

zip:
	@zip -rX releases/$(version).zip dist

watch:
	@npm run watch

publish:
	@git tag ${version}
	@git push origin ${version}

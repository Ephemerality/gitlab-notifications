# 开发文档

## 数据存储

* `options.*` 用户配置的选项，例如 GitLab 服务器地址等。
* `meta.*` 程序运行时所需的元数据，例如最后的事件时间等。
* `data.*` 详细的时间数据信息。

## 事件类型

* issue-open
* issue-comment
* issue-assign
* issue-close
* mr-open
* mr-comment
* mr-assign
* mr-accepted
* mr-close

## Tools

- [Extensions Reloader](https://chrome.google.com/webstore/detail/extensions-reloader/fimgfedafeadlieiabdeeaodndnlbhid)

'use strict';

const util = require('../src/lib/util');
const should = require('should');
const $ = require('jquery');

describe('test/util.test.js', function() {

  it('sleep()', function*() {
    const timeStart = Date.now();
    yield util.sleep(500);
    const time = Date.now() - timeStart;
    time.should.greaterThanOrEqual(500);
  });

  it('hasOwnProperty()', function() {
    should(util.hasOwnProperty({}, '__key__')).eql(false);
    should(util.hasOwnProperty({__key__: 0}, '__key__')).eql(true);
  });

  it('unique()', function() {
    should(util.unique([])).eql([]);
    should(util.unique([0, 1, 2])).eql([0, 1, 2]);
    should(util.unique([0, 1, 2, 1, 0, 2])).eql([0, 1, 2]);
    should(util.unique([0, 1, 2, 1, '0', 2])).eql([0, 1, 2, '0']);
  });

  it('toArray()', function() {
    should(arguments).not.eql([]);
    should(util.toArray(arguments)).eql([]);

    should(document.getElementsByClassName('.not-exists-class')).not.eql([]);
    should(util.toArray(document.getElementsByClassName('.not-exists-class'))).eql([]);

    should(document.querySelector('.not-exists-class')).not.eql([]);
    should(util.toArray(document.querySelector('.not-exists-class'))).eql([]);

    should($('.not-exists-class')).not.eql([]);
    should(util.toArray($('.not-exists-class'))).eql([]);
  });
});

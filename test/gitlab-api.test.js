'use strict';

const $ = require('jquery');
require('jquery-mockjax')($, window);
require('should');

const GitLabAPI = require('../src/gitlab-api');
const mockdata = require('./mockdatas/');

// TODO: gitlab api unit-tests.

describe('gitlab-api', function() {

  let api;
  before(() => {
    $.mockjax({
      url: 'https://example.com/api/v3/projects',
      contentType: 'application/json',
      // FIXME: mock 的 header，$.ajax 的 xhr 对象中取不到。
      headers: {
        Link: `
<https://example.com/api/v3/projects?page=1&per_page=20>; rel="prev",
<https://example.com/api/v3/projects?page=1&per_page=20>; rel="next",
<https://example.com/api/v3/projects?page=1&per_page=20>; rel="first",
<https://example.com/api/v3/projects?page=1&per_page=20>; rel="last"
        `,
      },
      responseText: mockdata.projects,
    });
    $.mockjax({
      url: 'https://example.com/api/v3/projects/3/issues/30001/notes/3111001',
      contextType: 'application/json',
      responseText: mockdata.notes_1,
    });

    api = new GitLabAPI({
      server: 'https://example.com',
      privateToken: '***',
    });
  });

  it('api.porjects()', function*() {
    const projects = yield api.projects();
    projects.length.should.be.eql(7);
  });

  it('api.parse("gitlab/gitlabhq")', function*() {
    const project = yield api.parse('gitlab/gitlabhq');
    project.should.be.eql(mockdata.projects[0]);
  });

  it.skip('api.issueNote()', function*() {
    const note = yield api.issueNote(3, 91, 152887);
    console.log('issue note', note);
  });

  // const mrNote = yield api.mergeRequestNote(proj.id, 799, 153041);
  // console.log('mr note', mrNote);

  // const issue = yield api.issue(proj.id, 1);
  // console.log('issue', issue);

  // const issueNotes = yield api.issueNotes(proj.id, 1);
  // console.log('notes', issueNotes);

  // const mr = yield api.mergeRequest(proj.id, 1);
  // console.log('mr', mr);

  // const mrNotes = yield api.mergeRequestNotes(proj.id, 1);
  // console.log('mr notes', mrNotes);

  // test comment on mr.
  // const noted = yield api.commentMR(proj.id, 1, '**test bold**');
  // console.log('comment on mr', noted);
});

'use strict';

require('./main.css');

const loc = window.location;
const $ = require('jquery');

const Events = require('events').EventEmitter;
const i18n = require('./lib/i18n');
// const Notification = require('./gitlab-notifications');
// const Dashboard = require('./dashboard');
const Options = require('./contentoptions');

const _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-10258428-4']);
_gaq.push(['_trackPageview']);
require('./lib/ga');

Options.get('gitlab.accounts')
.then(function(GitLab_accounts) {
  if (!GitLab_accounts) { return; }

  if (!GitLab_accounts.findIndex(account => account.serverUrl.startsWith(loc.origin)) < 0) { return; }

  window.popStateHandler = new Events();

  (function() {
    let previousUrl = loc.origin + loc.pathname + loc.search;
    setInterval(function() {
      const url = loc.origin + loc.pathname + loc.search;
      if (previousUrl !== url) {
        window.popStateHandler.emit('statechange', {
          length: window.history.length,
          scrollRestoration: 'auto',
          state: {
            trubolinks: true,
            url: url,
          },
          originState: {
            trubolinks: true,
            url: previousUrl,
          },
        });

        previousUrl = url;
      }
    }, 100);
  })();

  chrome.extension.onRequest.addListener(function(request) {
    if (request.hasOwnProperty('type')) {
      switch (request.type) {
      case 'open':
        console.log(i18n.get('open'));
        break;
      default:
        break;
      }
    }
  });

  window.popStateHandler.on('statechange', init);
  init();
});

const RE_ISSUE = /^(https?:\/\/[^\/]+\/[^\/]+\/[^\/]+\/issues\/\d+)/;
const RE_MR = /^(https?:\/\/[^\/]+\/[^\/]+\/[^\/]+\/merge_requests\/\d+)/;
function readEvents(url) {
  let key;
  [RE_ISSUE, RE_MR].some(re => {
    let m = re.exec(url);
    if (m) {
      key = m[1];
      return true;
    }
  });
  if (!key) return;
  chrome.extension.sendRequest({type: 'mark-as-read', key: key}, function() {
  });

  chrome.extension.sendRequest({type: 'is-mute', key: key}, function(isMute) {
    $('form.issuable-context-form').append(`<div class="block">
        <div class="title"><label class="light">GitLab Notifications</label></div>
        <button class="btn btn-block btn-gray gitlab-notifications-mute-button" type="button">
          <span>${ isMute ? 'Unmute' : 'Mute' }</span>
        </button>
      </div>`);
    $('.gitlab-notifications-mute-button').click(function() {
      const action = isMute ? 'mark-as-unmute' : 'mark-as-mute';
      chrome.extension.sendRequest({type: action, key: key}, function() {
        isMute = !isMute;
        $('.gitlab-notifications-mute-button').text(isMute ? 'Unmute' : 'Mute');
      });
    });
  });
}

// 初始化页面中需要插如的 HTML 元素，提升用户体验。
function init() {

  // 访问页面时，通知中心中标记为已读。
  readEvents(loc.href);

  // const pathname = loc.pathname;

  // // init notification icon.
  // if ($('#__GitLabNotificationIcon__').length === 0) {
    // $('<li><a id="__GitLabNotificationIcon__" href="/notifications" title="Notifications"><i class="gitlab-notification-icon"></i></a></li>')
      // .appendTo('.navbar-nav')
      // .click(function(evt) {
        // window.history.pushState({
          // title: 'Notification',
          // url: '/notifications',
        // }, 'Notification', '/notifications');

        // evt.stopPropagation();
        // return false;
      // });
  // }

  // if (pathname === '/' && loc.hash === '#notifications') {
    // return window.history.replaceState({
      // title: 'Notification',
      // url: '/notifications',
    // }, 'Notification', '/notifications');
  // } else if (pathname === '/notifications') {
    // return Notification.init();
  // } else if (pathname.startsWith('/dashboard/issues')) {
    // return Dashboard.Issues.init({
      // repository: '/dashboard',
    // });
  // } else if (pathname.startsWith('/dashboard/merge_requests')) {
    // return Dashboard.MergeRequest.init({
      // repository: '/dashboard',
    // });
  // }

  // const match_issue = /^(\/[\w-]+\/[\w-]+)\/issues/.exec(pathname);
  // if (match_issue) {
    // return Dashboard.Issues.init({
      // repository: match_issue[1],
    // });
  // }

  // const match_mr = /^(\/[\w-]+\/[\w-]+)\/merge_requests/.exec(pathname);
  // if (match_mr) {
    // return Dashboard.MergeRequest.init({
      // repository: match_mr[1],
    // });
  // }
}

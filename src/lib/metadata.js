'use strict';

const Store = require('./store');
const prefix = 'metadata.';

const metadata = {
  get: function(name) {
    return Store.get(prefix + name);
  },
  set: function(name, value) {
    Store.set(prefix + name, value);
  },
};

class Metadata {
  constructor(serverUrl) {
    this._prefix = serverUrl + '.';
  }
  get(name) {
    return metadata.get(this._prefix + name);
  }
  set(name, value) {
    metadata.set(this._prefix + name, value);
  }
}

module.exports = Metadata;

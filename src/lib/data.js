'use strict';

const Store = require('./store');
const prefix = 'data.';

const Data = {
  get: function(name) {
    return Store.get(prefix + name);
  },
  set: function(name, value) {
    Store.set(prefix + name, value);
  },
};

module.exports = Data;

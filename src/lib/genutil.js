'use strict';

function* partChunks(genList, partLen = 5) {
  let index = 0;
  const allList = [];
  do {
    const partList = yield genList.slice(index, index + partLen);
    allList.push.apply(allList, partList);
    index += partList.length;
  } while (index < genList.length);
  return allList;
}

module.exports = {
  partChunks,
};

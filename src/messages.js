'use strict';

const Store = require('./lib/store');

const PREFIX = 'notification.data';
const DEFAULT_LIST_OPTIONS = {
  type: '*',
};

const Messages = {
  // 列出所有的消息
  list: function(filter) {
    filter = Object.assign({}, DEFAULT_LIST_OPTIONS, filter);
    const messages = Store.get(PREFIX) || [];
    switch (filter.type) {
    case 'unread':
    case 'all':
      return messages.filter(msg => !msg.read);
    case '*':
      return messages;
    default:
      throw new Error('Unknow messages filter type: ' + filter.type);
    }
  },

  // 添加新的 Issue 消息。
  // - 如果消息已存在：
  //   - 如果消息是未读的：
  //     - 如果有状态更新，更新状态。
  //     - 如果有参与更新，更新参与性。
  //   - 否则，消息是已读的：
  //     - 标记为未读。
  //     - 更新状态。
  //     - 更新参与性。
  // - 否则，如果消息不存在：
  //   - 标记消息为未读。
  //   - 添加新的消息。
  add: function(message) {
    let messages = Messages.list({type: '*'});
    // 相同的消息是否已经存在。
    // 消息的 key 相同，则消息相同。
    const existsMsgIndex = messages.findIndex(msg => msg.key === message.key);
    const isMsgExists = existsMsgIndex >= 0;
    let changed = false;
    if (isMsgExists) {
      const msg = messages[existsMsgIndex];
      if (msg.mute) {
        return;
      }
      if (msg.read) {
        msg.read = false;
        msg.url = message.url;
        msg.timestamp = message.timestamp;
        changed = true;
      }
      if (message.participating && !msg.participating) {
        msg.participating = true;
        changed = true;
      }
      if (message.status && message.status !== msg.status) {
        msg.status = message.status;
        msg.statusCode = message.statusCode;
        changed = true;
      }
    } else {
      message.read = false;
      messages.push(message);
      changed = true;
    }

    if (changed) {
      Store.set(PREFIX, messages);
    }
  },

  // 如果事件已经存在，更新事件的 participating 状态。
  // 适用场景：
  // - 自己发出的消息，需要更新参与、状态等。
  // - 未读消息已经存在，且 participating 标记变更。
  // - Issue, MR 的状态变更。
  update: function(message) {
    let messages = Messages.list({type: '*'});
    // 相同的消息是否已经存在。
    // 消息的 key 相同，则消息相同。
    const existsMsgIndex = messages.findIndex(msg => msg.key === message.key);
    const isMsgExists = existsMsgIndex >= 0;
    let changed = false;
    if (isMsgExists) {
      const msg = messages[existsMsgIndex];
      if (msg.mute) {
        return;
      }
      if (message.participating && !msg.participating) {
        msg.participating = true;
        changed = true;
      }
      if (message.status && message.status !== msg.status) {
        msg.status = message.status;
        changed = true;
      }
    } else {
      message.read = true;
      messages.push(message);
      changed = true;
    }

    if (changed) {
      Store.set(PREFIX, messages);
    }
  },

  // Make message as read, without remove message.
  // @param {String} key: 指定消息 的 key。
  read: function(key) {
    const messages = Messages.list({type: '*'});
    const index = messages.findIndex(function(msg) {
      return msg.key === key;
    });
    if (index === -1) return;
    messages[index].read = true;
    Store.set(PREFIX, messages);
  },

  isMute: function(key) {
    const msg = Messages.list({type: '*'}).find(msg => msg.key === key);
    return msg && msg.mute;
  },

  // Make message as read and mute.
  // @param {String} key: 指定消息 的 key。
  // @param {Boolean} mute: 是否静音，默认为 true。
  mute: function(key, mute = true) {
    const messages = Messages.list({type: '*'});
    const index = messages.findIndex(function(msg) {
      return msg.key === key;
    });
    if (index === -1) return;
    // 如果置为静音，则同时置为已读。
    if (mute) {
      messages[index].read = mute;
    }
    messages[index].mute = mute;
    Store.set(PREFIX, messages);
  },

  unmute: function(key) {
    Messages.mute(key, false);
  },

  // 标记指定仓库(project_url)的所有消息为已读。
  // @param {String} repo: 指定消息的 repo (project_url)。
  readRepo: function(project_url) {
    const messages = Messages.list({type: '*'});
    for (let i = messages.length - 1, msg; i >= 0; i--) {
      msg = messages[i];
      if (!msg.read && msg.project_url === project_url) {
        msg.read = true;
      }
    }
    Store.set(PREFIX, messages);
  },

  // 标记所有消息为已读。
  readAll: function() {
    const messages = Messages.list({type: '*'});
    for (let i = messages.length - 1; i >= 0; i--) {
      if (!messages[i].read) {
        messages[i].read = true;
      }
    }
    Store.set(PREFIX, messages);
  },

  /**
   * 返回消息的条目数量。
   * @param {Enum} type 消息条目数量。
   *   - all 全部的
   *   - watching: 关注的
   *   - participating 参与的
   * @return {Number} 指定类型消息条目数。
   */
  length: function(type = 'all') {
    let messages = Messages.list({type: 'unread'});
    if (type === 'participating') {
      return messages.filter(msg => msg.participating).length;
    } else if (type === 'watching') {
      return messages.filter(msg => msg.watching).length;
    } else if (type === 'all') {
      return messages.length;
    } else {
      throw new Error('[Message.length] Not support type "' + type + '".');
    }
  },
};

module.exports = Messages;

'use strict';

const Events = require('events').EventEmitter;
const GitLabAPI = require('./gitlab-api');
const Options = require('./lib/options');
const Timeouter = require('./timeouter');
const util = require('./lib/util');
const genutil = require('./lib/genutil');
const monitor = require('./lib/monitor');

const KEY_LAST_UNWATCH_EVENT_TIME = 'last_unwatch_event_time';
const DEFAULT_LAST_EVENT_TIME = Date.now() - 6 * 60 * 60 * 1000;

class GitLabEvents extends Events {

  constructor(account) {
    super();
    this.account = account;
    this.api = new GitLabAPI({
      server: account.serverUrl,
      path: '/api/v3',
      token: account.privateToken,
    });
    this._key_last_unwatch_event_time = this.api.serverUrl + '-' + KEY_LAST_UNWATCH_EVENT_TIME;
    this.last_unwatch_event_time = Options.get(this._key_last_unwatch_event_time) || DEFAULT_LAST_EVENT_TIME;

    this._keyLastProjectEventTime = this.api.serverUrl + '-last-project-event-time';
    this.lastProjectEventTime = Options.get(this._keyLastProjectEventTime) || DEFAULT_LAST_EVENT_TIME;

    this._timeout = new Timeouter(30 * 1000, 10 * 60 * 1000);

    this._running = true;
  }

  * start() {
    this._running = true;

    try {
      const gitlabVersion = yield this.api.gitlabVersion();
      monitor.log(gitlabVersion, 'gitlab.version');
    } catch (ex) {
      monitor.error(ex, 'GET gitlab version error');
    }

    return yield this.loop();
  }

  stop() {
    this._running = false;
  }

  * getNoteEvents() {
    const PER_PAGE = 20;
    let page_offset = 0;
    let foundLast = false;
    let all_events = [];

    do {
      const events = yield this.api.liveEvents(PER_PAGE, page_offset);

      if (!events.length) {
        foundLast = true;
        break;
      }

      for (const event of events) {

        if (foundLast) break;

        const proj = yield this.api.parse(event.project_path);
        if (!proj) break;

        if (event.type.startsWith('issue-')) {
          const issues = yield this.api.issue(proj.id, event.issue_iid);
          const issue = issues[0];
          if (issue) {
            event.title = issue.title;
            event.status = issue.state;
            event.statusCode = 'issue-' + issue.state;
            event.topic_author_name = issue.author.name;
            event.topic_author_username = issue.author.username;
            event.topic_author_avatar = issue.author.avatar_url;

            if (issue.assignee) {
              event.assignee_username = issue.assignee.username;
              event.assignee_name = issue.assignee.name;
              event.assignee_avatar = issue.assignee.avatar_url;
            }
          }

          if (event.type === 'issue-comment') {
            const note = yield this.api.issueNote(proj.id, event.issue_iid, event.note_id);
            if (note) {
              event.body = note.body;
              event.author_name = note.author.name;
              event.author_username = note.author.username;
              event.author_avatar = note.author.avatar_url;
              event.timestamp = Date.parse(note.created_at);
            }
          } else if (event.type === 'issue-opened') {
            event.status = 'opened';
            event.statusCode = 'issue-opened';
            event.body = '';
          } else if (event.type === 'issue-closed') {
            event.status = 'closed';
            event.statusCode = 'issue-closed';
            event.body = '';
          }
          // TODO: issue-reopened.

        } else if (event.type.startsWith('mergerequest-')) {

          const mrs = yield this.api.mergeRequest(proj.id, event.mr_iid);
          const mr = mrs[0];
          if (mr) {
            event.title = mr.title;
            event.status = mr.state;
            event.statusCode = 'mergerequest-' + mr.state;
            event.topic_author_name = mr.author.name;
            event.topic_author_username = mr.author.username;
            event.topic_author_avatar = mr.author.avatar_url;

            if (mr.assignee) {
              event.assignee_username = mr.assignee.username;
              event.assignee_name = mr.assignee.name;
              event.assignee_avatar = mr.assignee.avatar_url;
            }
          }

          if (event.type === 'mergerequest-comment') {
            const note = yield this.api.mergeRequestNote(proj.id, event.mr_iid, event.note_id);
            event.body = note.body;
            event.author_name = note.author.name;
            event.author_username = note.author.username;
            event.author_avatar = note.author.avatar_url;
            event.timestamp = Date.parse(note.created_at);
          } else if (event.type === 'mergerequest-opened') {
            event.status = 'opened';
            event.statusCode = 'mergerequest-opened';
            event.body = mr.description;
          } else if (event.type === 'mergerequest-accepted') {
            event.status = 'accepted';
            event.statusCode = 'mergerequest-accepted';
            event.body = '';
          } else if (event.type === 'mergerequest-closed') {
            event.status = 'closed';
            event.statusCode = 'mergerequest-closed';
            event.body = '';
          }
          // TODO: mergerequest-reopened.

        } else {
          break;
        }

        if (event.timestamp <= this.last_unwatch_event_time) {
          foundLast = true;
          break;
        }

        all_events.push(event);
      }

      page_offset += PER_PAGE;

    } while (!foundLast);

    const watchingProjects = this.account.watching_projects;
    if (watchingProjects && watchingProjects.length) {
      all_events = all_events.filter(event => {
        return watchingProjects.findIndex(proj => proj.project_url === event.project_url) === -1;
      });
    }

    all_events.forEach(evt => evt.watching = false);
    all_events = all_events.sort((a, b) => {
      return a.timestamp - b.timestamp;
    });
    if (all_events && all_events.length) {
      const lastEvent = all_events[all_events.length - 1];
      this.last_unwatch_event_time = lastEvent.timestamp;
      Options.set(this._key_last_unwatch_event_time, lastEvent.timestamp);
    }

    return all_events;
  }

  * _getSingleProjectEvents(project, lastEventTime) {
    const newEvents = [];
    const PER_PAGE = 20;
    let page = 1;
    let foundLastEvent = false;

    // push new events untill found the last fetched event.
    function pushNewEvent(evt) {
      if (foundLastEvent) { return true; }
      if (evt.timestamp <= lastEventTime) {
        foundLastEvent = true;
        return true;
      }
      newEvents.push(evt);
    }

    do {
      const projectEvents = yield this.api.projectEvents(project.id, page++, PER_PAGE);
      projectEvents.map(parseProjectEvents)
        .filter(evt => !!evt)
        .some(pushNewEvent);
      if (projectEvents.length === 0) break;
    } while (!foundLastEvent);

    const completionEvents = yield newEvents.map(evt => this._completionEvent(evt, project));

    return completionEvents.filter(e => !!e);
  }

  // 补全事件信息。
  * _completionEvent(evt, project) {
    switch (evt.type) {
    case 'issue-opened':
    case 'issue-closed':
    case 'issue-comment':
      const issue = yield this.api.issue_1(evt.project_id, evt.topic_id);
      evt.title = issue.title;
      evt.key = project.web_url + '/issues/' + issue.iid;
      evt.url = project.web_url + '/issues/' + issue.iid + '#note_' + evt.target_id;
      // TODO: evt.participating
      evt.project_path = project.path_with_namespace;
      evt.project_url = project.web_url;
      if (issue.assignee) {
        evt.assignee_name = issue.assignee.name;
        evt.assignee_username = issue.assignee.username;
        evt.assignee_avatar = issue.assignee.avatar_url;
      }
      evt.status = issue.state;
      evt.statusCode = 'issue-' + issue.state;
      break;

    case 'mergerequest-opened':
    case 'mergerequest-closed':
    case 'mergerequest-accepted':
    case 'mergerequest-comment':
      const mr = yield this.api.mergeRequest_1(evt.project_id, evt.topic_id);
      evt.title = mr.title;
      evt.key = project.web_url + '/merge_requests/' + mr.iid;
      evt.url = project.web_url + '/merge_requests/' + mr.iid + '#note_' + evt.target_id;
      // TODO: evt.participating
      evt.project_path = project.path_with_namespace;
      evt.project_url = project.web_url;
      if (mr.assignee) {
        evt.assignee_name = mr.assignee.name;
        evt.assignee_username = mr.assignee.username;
        evt.assignee_avatar = mr.assignee.avatar_url;
      }
      const state = mr.state === 'merged' ? 'accepted' : mr.state;
      evt.status = state;
      evt.statusCode = 'mergerequest-' + state;
      break;

    default:
      return null;
    }

    switch (evt.type) {
    case 'issue-opened':
    case 'mergerequest-opened':
      evt.status = 'opened';
      evt.statusCode = evt.type;
      break;
    case 'issue-closed':
    case 'mergerequest-closed':
      evt.status = 'closed';
      evt.statusCode = evt.type;
      break;
    case 'mergerequest-accepted':
      evt.status = 'accepted';
      evt.statusCode = evt.type;
      break;
    }

    return evt;
  }

  /**
   */
  * getProjectEvents() {
    const projects = this.account.watching_projects || [];
    if (!projects || !projects.length) return [];

    const eventsResult = yield genutil.partChunks(
      projects.map(proj => this._getSingleProjectEvents(proj, this.lastProjectEventTime))
    );
    // merge all projects event.
    let mergedEvents = [];
    eventsResult.forEach(oneProjectEvents => {
      mergedEvents.push.apply(mergedEvents, oneProjectEvents);
    });
    mergedEvents = mergedEvents.filter(event => event.timestamp > this.lastProjectEventTime);

    // has new events.
    if (mergedEvents.length) {
      const events = mergedEvents.sort((a, b) => {
        return a.timestamp - b.timestamp;
      });
      this.lastProjectEventTime = events[events.length - 1].timestamp;
      Options.set(this._keyLastProjectEventTime, this.lastProjectEventTime);

      events.forEach(evt => evt.watching = true);

      return events;
    }

    return [];
  }

  * loop() {
    if (!this._running) {
      console.log('GitLab Notifications stoped.');
      return;
    }

    try {
      const eventsResponse = yield {
        watching: this.getProjectEvents(),
        unwatching: this.getNoteEvents(),
      };
      const mergedEvents = eventsResponse.watching.concat(eventsResponse.unwatching);
      const events = mergedEvents.sort((a, b) => {
        return a.timestamp - b.timestamp;
      });
      if (events && events.length) {
        this.emit('events', events);
      }
      if (this.account.state !== 'ok') {
        this.account.state = 'ok';
        this.emit('account.state.change', this.account.state);
      }
      this._timeout.reset();
    } catch (ex) {
      console.error('GET events error:', ex);
      ex.message = 'GET events error: ' + ex.message;
      monitor.error(ex);
      if (this.account.state !== 'fail') {
        this.account.state = 'fail';
        this.emit('account.state.change', this.account.state);
      }
      this._timeout.inc();
    }
    yield util.sleep(this._timeout.valueOf());
    yield this.loop();
  }

}

function parseProjectEvents(event) {
  let evt = {};
  switch (event.action_name) {

  // commented on Issue
  // commented on MergeRequest
  // commented on Commit
  case 'commented on':
    if (!event.note || event.note.noteable_type === 'Commit') return null;

    evt.type = (event.note.noteable_type + '-comment').toLowerCase();
    evt.author_avatar = event.author.avatar_url;
    evt.author_name = event.author.name;
    evt.author_username = event.author.username;
    evt.body = event.note.body;
    evt.timestamp = Date.parse(event.created_at);
    evt.project_id = event.project_id;
    evt.target_id = event.target_id;
    evt.topic_id = event.note.noteable_id;
    evt.title = '';
    break;

  // opened Issue
  // opened MergeRequest
  case 'opened':
    evt.type = (event.target_type + '-opened').toLowerCase();
    evt.author_avatar = event.author.avatar_url;
    evt.author_name = event.author.name;
    evt.author_username = event.author.username;
    // TODO: after filter un-read notifications, yield get body.
    evt.body = event.target_type + ' Opened.';
    evt.timestamp = Date.parse(event.created_at);
    evt.title = event.target_title;
    evt.project_id = event.project_id;
    evt.target_id = event.target_id;
    evt.topic_id = event.target_id;
    break;

  // closed Issue
  // closed MergeRequest
  // closed Milestone
  case 'closed':
    evt.type = (event.target_type + '-closed').toLowerCase();
    evt.author_avatar = event.author.avatar_url;
    evt.author_name = event.author.name;
    evt.author_username = event.author.username;
    evt.body = event.target_type + ' Closed.';
    evt.timestamp = Date.parse(event.created_at);
    evt.project_id = event.project_id;
    evt.title = event.target_title;
    evt.target_id = event.target_id;
    evt.topic_id = event.target_id;
    break;

  // accepted MergeRequest
  case 'accepted':
    evt.type = (event.target_type + '-accepted').toLowerCase();
    evt.author_avatar = event.author.avatar_url;
    evt.author_name = event.author.name;
    evt.author_username = event.author.username;
    evt.body = event.target_type + ' Accepted.';
    evt.timestamp = Date.parse(event.created_at);
    evt.project_id = event.project_id;
    evt.title = event.target_title;
    evt.target_id = event.target_id;
    evt.topic_id = event.target_id;
    break;

  // case 'deleted':
  // case 'pushed to':
  // case 'pushed new':
  default:
    return null;
  }

  return evt;
}

module.exports = GitLabEvents;
